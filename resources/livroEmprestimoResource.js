"use strict";
class LivroEmprestimoResource {

    findAll(req, res, next) {
        const LivroEmprestimoBusiness = require("../business/livroEmprestimoBusiness");
        this.livroEmprestimoBusiness = new LivroEmprestimoBusiness();
        this.livroEmprestimoBusiness.findAll(req.params,
            function callbackLivroEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    post(req, res, next) {
        const LivroEmprestimoBusiness = require("../business/livroEmprestimoBusiness");
        this.livroEmprestimoBusiness = new LivroEmprestimoBusiness();
        this.livroEmprestimoBusiness.post(req.body,
            function callbackLivroEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    update(req, res, next) {
        const LivroEmprestimoBusiness = require("../business/livroEmprestimoBusiness");
        this.livroEmprestimoBusiness = new LivroEmprestimoBusiness();
        this.livroEmprestimoBusiness.update(req.body,
            function callbackLivroEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    delete(req, res, next) {
        const LivroEmprestimoBusiness = require("../business/livroEmprestimoBusiness");
        this.livroEmprestimoBusiness = new LivroEmprestimoBusiness();
        this.livroEmprestimoBusiness.delete(req.body,
            function callbackLivroEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }
}
module.exports = LivroEmprestimoResource;
