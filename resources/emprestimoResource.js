"use strict";
class EmprestimoResource {


    buscaId(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.buscaId(req.params,
            function callbackEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    findAll(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.findAll(req.params,
            function callbackEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    post(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.post(req.body,
            function callbackEmprestimo(rows, err = null) {
                if (rows.length > 0) {
                    res.json(rows);
                    next();
                } else {
                    res.send(err);
                    next();
                }
            }
        );
    }

    update(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.update(req.body,
            function callbackEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }

    delete(req, res, next) {
        const EmprestimoBusiness = require("../business/emprestimoBusiness");
        this.emprestimoBusiness = new EmprestimoBusiness();
        this.emprestimoBusiness.delete(req.body,
            function callbackEmprestimo(rows) {
                res.json(rows); next();
            }
        );
    }
}
module.exports = EmprestimoResource;
