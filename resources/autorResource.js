"use strict";
class AutorResource {
    
    findAll(req, res, next) {
        const AutorBusiness = require("../business/autorBusiness");
        this.autorBusiness = new AutorBusiness();
        this.autorBusiness.findAll(
            function callbackAutor(rows) {
                res.json(rows); next();
            }
        );
    }

    post(req, res, next) {
        const AutorBusiness = require("../business/autorBusiness");
        this.autorBusiness = new AutorBusiness();
        this.autorBusiness.post(req.body,
            function callbackAutor(rows) {
                res.json(rows); next();
            }
        );
    }

    update(req, res, next) {
        const AutorBusiness = require("../business/autorBusiness");
        this.autorBusiness = new AutorBusiness();
        this.autorBusiness.update(req.body,
            function callbackAutor(rows) {
                res.json(rows); next();
            }
        );
    }

    delete(req, res, next) {
        const AutorBusiness = require("../business/autorBusiness");
        this.autorBusiness = new AutorBusiness();
        this.autorBusiness.delete(req.params,
            function callbackAutor(rows) {
                res.json(rows); next();
            }
        );
    }
}
module.exports = AutorResource;
