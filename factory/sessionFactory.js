//Versão Antiga
/*"use strict";
class SessionFactory {
    constructor() {
        var mysql    = require('mysql');
        this.connection = mysql.createConnection({
            host: 'unipbiblio.mysql.dbaas.com.br',
            user: 'unipbiblio',
            password: 'Unip123#',
            database: 'unipbiblio'
        });
    }

    connect() {
        this.connection.connect();
    } 
    
    end() {
        if (this.connection)
            this.connection.end();
    }
}

module.exports = SessionFactory;*/

"use strict";
class SessionFactory {
    constructor() {
        var mysql = require('mysql');
        this.connection = mysql.createConnection({
            host: 'unipbiblio.mysql.dbaas.com.br',
            user: 'unipbiblio',
            password: 'Unip123#',
            database: 'unipbiblio'
        });
    }

    connect(callbackConnect) {

        this.connection.connect(function () {
            callbackConnect()
        });
    }

    end() {
        if (this.connection)
            this.connection.end();
    }
}

module.exports = SessionFactory;