"use strict";
var errs = require('restify-errors');

class EmprestimoBusiness {

    buscaId(usuario, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query(' SELECT max(emId) AS emId, usId, emDataInicio, emDataFim, emDataDevolucao FROM Emprestimo WHERE usId =' + usuario.usId + ';',
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    findAll(usuario, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query(' SELECT * FROM Emprestimo JOIN LivroEmprestimo ON Emprestimo.emId = LivroEmprestimo.emId JOIN Livro ON LivroEmprestimo.lvId = Livro.lvId WHERE Emprestimo.usId =' + usuario.usId + ';',
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    post(emprestimo, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query(' SELECT * FROM Livro WHERE lvId = ' + emprestimo.lvId + ' AND lvQuantidade > 0',
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    //callbackEmprestimo(rows);
                    if (rows.length > 0) {
                        that.session.connection.query("INSERT INTO Emprestimo (usId, emDataInicio, emDataFim) values (" + emprestimo.usId + ", CURDATE(), DATE_ADD(CURDATE(), INTERVAL 10 DAY))",
                            function (err, rows, fields) {
                                if (err)
                                    throw err;
                                callbackEmprestimo(rows);
                                /*if (that.session)
                                    that.session.end();*/
                            });
                    } else {
                        var err = new errs.BadRequestError("Livro Indisponível");
                        callbackEmprestimo([], err);
                    }

                    if (that.session)
                        that.session.end();
                });
        });
    }

    update(emprestimo, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("UPDATE Emprestimo SET emDataDevolucao ='" + emprestimo.emDataDevolucao + "' WHERE emId = " + emprestimo.emId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }
    
    delete(emprestimo, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("DELETE FROM Emprestimo WHERE emId = " + emprestimo.emId + " AND emDataDevolucao <> 0000-00-00 OR emDataDevolucao <> null",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }
}
module.exports = EmprestimoBusiness;